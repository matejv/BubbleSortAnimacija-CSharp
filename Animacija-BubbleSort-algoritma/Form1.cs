﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Animacija_BubbleSort_algoritma
{
    public partial class Form1 : Form
    {
        Button[] gumbi = new Button[10];
        int primerjave = 0, zamenjave = 0;

        public Form1()
        {
            InitializeComponent();
            gumbi[0] = button1;
            gumbi[1] = button2;
            gumbi[2] = button3;
            gumbi[3] = button4;
            gumbi[4] = button5;
            gumbi[5] = button6;
            gumbi[6] = button7;
            gumbi[7] = button8;
            gumbi[8] = button9;
            gumbi[9] = button10;
        }

        private void button11_Click(object sender, EventArgs e)
        {
            Random r = new Random();
            for (int i = 0; i < 10; i++)
                gumbi[i].Text = r.Next(100).ToString();
        }

        private void button12_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < 10; i++)
            {
                for (int j = 9; j > i; j--)
                {
                    gumbi[j].BackColor = Color.Orange;
                    gumbi[j - 1].BackColor = Color.Orange;
                    Thread.Sleep(500); //pavzira UI in delovanje za pol sekunde
                    primerjave++;
                    if (Convert.ToInt32(gumbi[j].Text) < Convert.ToInt32(gumbi[j - 1].Text))
                    {
                        string temp = gumbi[j].Text;
                        gumbi[j].Text = gumbi[j - 1].Text;
                        gumbi[j - 1].Text = temp;
                        zamenjave++;
                    }
                    label3.Text = primerjave.ToString();
                    label4.Text = zamenjave.ToString();
                    this.Refresh();
                    Thread.Sleep(1000); //pavzira UI in delovanje za eno sekundo
                    gumbi[j].BackColor = SystemColors.Control;
                    gumbi[j - 1].BackColor = SystemColors.Control;

                    //gumbi[i].ForeColor = Color.Red;
                    //this.Refresh();
                    //Thread.Sleep(1000);
                }
                gumbi[i].BackColor = Color.Gray; //gumb, ki ga dokončno sortira oz. postavi na zadnje/ustrezno mesto obarva sivo in čezenj ne gre več (pogoji v zankah)
            }
        }
    }
}
